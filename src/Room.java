import java.awt.List;
import java.util.ArrayList;

/**
 * Room.java
 * modified from Sebastien's Code
 * 
 */
public class Room {
	public static final int ROWS = 6;
	private int x;
	private int y;
	private Room[] neighbors;
	private boolean pit;
	private boolean dragon;
	private boolean gold;
	private boolean hintShown;
	private String hints;
	private ArrayList<Character> dangers = new ArrayList<Character>();
	private boolean isSafe = false;
	private boolean visited = false;
	private double wumpusRisk;
	private double pitRisk;
	private double visitedRisk;

	public Room (int x, int y) {
		this.x = x;
		this.y = y;
		this.pit = false;
		this.dragon = false;
		this.gold = false;
		neighbors = new Room[4];
		this.hintShown = false;
		this.hints = "";
		this.isSafe = false;
		this.wumpusRisk = 0;
		this.pitRisk = 0;
		this.visitedRisk = 0;
	}
	
	/**
	 * Gets the risk value of the room
	 * @return - the value of the wumpus risk
	 */
	public double getWumpusRisk() {
		return this.wumpusRisk;
	}
	
	/**
	 * Sets the risk value of the room
	 * @param value - the value of the wumpus risk
	 */
	public void setWumpusRisk(double value) {
		this.wumpusRisk = value;
	}
	
	/**
	 * Gets the pit risk value
	 * @return - the value of the pit risk
	 */
	public double getPitRisk() {
		return this.pitRisk;
	}
	
	/**
	 * Sets the pit risk value of the room
	 * @param value - the value to set
	 */
	public void setPitRisk (double value) {
		this.pitRisk = value;
	}
	
	/**
	 * Gets the visited risk value
	 * @return - the value of the visited risk
	 */
	public double getVisitedRisk() {
		return this.visitedRisk;
	}
	
	/**
	 * Sets the visited risk value of the room
	 * @param value - the value to set
	 */
	public void setVisitedRisk (double value) {
		this.visitedRisk = value;
	}
	
	public double getTotalRisk() {
		return this.wumpusRisk + this.pitRisk + this.visitedRisk;
	}
	
	/**
	 * lets you know if this room has been visited
	 * @return
	 */
	public boolean getVisited() {
		return this.visited;
	}
	
	/**
	 * Sets the room as visited
	 */
	public void setVisited() {
		this.visited = true;
	}
	
	/**
	 * Unsets the visited property
	 */
	public void unSetVisited() {
		this.visited = false;
	}
	
	/**
	 * gets the status of if the room is safe or not
	 * @return
	 */
	public boolean getSafe() {
		return this.isSafe;
	}
	
	/**
	 * sets the status of if the room is safe or not.
	 */
	public void setSafe() {
		this.isSafe = true;
	}
	
	/**
	 * Checks to see if the arraylist has the current danger. If not adds it to the list of dangers
	 * @param dangers
	 */
	public boolean setDangers (char dangers) {
		if (!this.dangers.contains(dangers)) {
			this.dangers.add(dangers);
			return true;
		} else
			return false;
	}
	
	public void removeDanger (char danger) {
		if (this.dangers.contains(danger)) {
//			this.dangers.remove(danger);
			this.dangers.clear();
		}
	}
	
	/**
	 * Returns an arraylist of dangers.
	 * @return
	 */
	public ArrayList<Character> getDangers() {
		return this.dangers;
	}

	/**
	 * 
	 */
	public void makePit(){
		this.pit = true;
	}

	/**
	 * Returns true if this room is a pit
	 * @return - true if a pit false if not a pit
	 */
	public boolean isPit(){
		return pit;
	}

	/**
	 * 
	 */
	public void spawnDragon(){
		this.dragon = true;
	}

	/**
	 * Sets the dragon variable to false
	 */
	public void killDragon(){
		this.dragon = false;
	}

	/**
	 * Returns true if this room has a dragon (wumpus)
	 * @return - true if there is a wumpus, false if not
	 */
	public boolean hasDragon(){
		return this.dragon;
	}

	/**
	 * Spawns gold in this room.
	 */
	public void depositGold(){
		this.gold = true;
	}

	/**
	 * If the room has gold then returns true. If room doesnt have gold returns false
	 * @return - true if there is gold, false if not
	 */
	public boolean grabGold(){
		if(this.gold==false)
			return false;
		this.gold = false;
		return true;
	}

	/**
	 * If the room has gold then returns true. If room doesnt have gold returns false
	 * @return - true if there is gold, false if not
	 */
	public boolean hasGold(){
		return this.gold;
	}

	/**
	 * Sets the neighorbors of the current room.
	 * @param rooms - 
	 */
	public void initNeighbors(Room[][] rooms){
		int len = rooms.length;//assume board is square
		neighbors[Board.WEST] = y==0?null:rooms[x][y-1];
		neighbors[Board.EAST] = y==len-1?null:rooms[x][y+1];
		neighbors[Board.NORTH] = x==0?null:rooms[x-1][y];
		neighbors[Board.SOUTH] = x==len-1?null:rooms[x+1][y];
	}

	/**
	 * Returns the rooms that are adjacent to the current room
	 * @return - an array of rooms that are next to the current room.
	 */
	public Room[] getNeighbors(){
		return neighbors;
	}

	/**
	 * Checks to see if the current rooms neighorbors has
	 * a dragon or a pit. If they do then set the appropriate
	 * hint and return a string of all of the hints (if any)
	 * @return - a string of hints for the room.
	 */
//	public String perceptions(){
////		ArrayList<Character> perceptions = new ArrayList<Character>();
//		hints = "";
//		boolean pitAir = false;
//		boolean stenchAir = false;
//		for (int i = 0; i < neighbors.length; i++){
//			if(neighbors[i]!=null){
//				if(neighbors[i].hasDragon())
//					stenchAir = true;
//				if(neighbors[i].isPit()){ 
//					pitAir = true;
//				}
//			}
//		}
//		
//		if (pitAir)
//			hints += "B ";
////			perceptions.add('B');
//		if (stenchAir)
//			hints += "S ";
////			perceptions.add('S');
//		if (hasDragon())
//			hints += "W ";
////			perceptions.add('W');
//		if (isPit())
//			hints += "P ";
////			perceptions.add('P');
//		if (hasGold())
//			hints += "G ";
////			perceptions.add('G');
////		System.out.println(hints);
//		return hints;
//	}
	
	/**
	 * Checks to see if the current rooms neighorbors has
	 * a dragon or a pit. If they do then set the appropriate
	 * hint and return a string of all of the hints (if any)
	 * @return - a string of hints for the room.
	 */
	public ArrayList<Character> perceptions(){
		ArrayList<Character> perceptions = new ArrayList<Character>();
		hints = "";
		boolean pitAir = false;
		boolean stenchAir = false;
		for (int i = 0; i < neighbors.length; i++){
			if(neighbors[i]!=null){
				if(neighbors[i].hasDragon())
					stenchAir = true;
				if(neighbors[i].isPit()){ 
					pitAir = true;
				}
			}
		}
		
		if (pitAir)
//			hints += "B ";
			perceptions.add('B');
		if (stenchAir)
//			hints += "S ";
			perceptions.add('S');
		if (hasDragon())
//			hints += "W ";
			perceptions.add('W');
		if (isPit())
//			hints += "P ";
			perceptions.add('P');
		if (hasGold())
//			hints += "G ";
			perceptions.add('G');
//		System.out.println(hints);
		return perceptions;
	}

	public void setHints(){
		hintShown = true;
	}

	public boolean isShown(){
		return hintShown;
	}

	/**
	 * Returns a number between 0 and 35 representing the 
	 * squares that are on the game board
	 * @return - a number between 0 and 35
	 */
	public int getLocation(){
		//	  System.out.println(ROWS*x+y);
		return ROWS*x+y;
	}
	
	public Room getRoom() {
		
		return this;
	}

	/*@Override
  public String ToString(){
   return "."+
    (dragon?"D ":"") + 
    (gold?"G ":"") +  
    (pit?"P ":"");
  }*/
}


/**
 * Player.java
 * modified from Sebastien's Code
 * 
 */
 public class Player
 {
  private Room currentRoom;
  private int dir;
  private boolean alive;
  private int arrow;
  private boolean gold;
  private boolean dragonKiller;
  
  public Player (Room start)
  {
   currentRoom = start;
   dir = Board.EAST;
   alive = true;
   arrow = 2;
   gold = false;
   dragonKiller = false;
  }
  
  public Player () {
	  
  }
  
  /**
   * Gets the neighorbors of the current room.
   * If neighorbor exists (IE not the edge of the board)
   * then the player can advance in the direction he is facing.
   * @return
   */
  public boolean forward(){
   Room next = currentRoom.getNeighbors()[dir]; //the next room in the direction the user is facing
   if(next==null){
    return false;
   }
   else{
    currentRoom = next;
    currentRoom.setHints();
    if (currentRoom.hasDragon() || currentRoom.isPit())
     alive = false;
   }
   return true;
  }
  
  public void turnLeft(){
   dir = dir==0?3:dir-1;
  }
  
  public void turnRight(){
   dir = dir==3?0:dir+1;
  }
  
  public Room getCurrentRoom(){
   return currentRoom;
  }
  
  public boolean hasGold(){
   return gold;
  }
  
  public boolean grabGold(){
   gold = currentRoom.grabGold();
   return gold;
  }
  
  public boolean isDragonKiller(){
   return dragonKiller;
  }
  
  public void resetDragonKiller(){
   dragonKiller = false;
  }
  
  public boolean isAlive(){
   return alive;
  }
  
  public boolean hasArrow(){
   if (arrow > 0)
     return true;
   return false;
  }
  
  public void shoot(){
   if(arrow==0)
    return;
   arrow --;
   Room r = currentRoom;
   while(r!=null){
    if(r.hasDragon()){
     r.killDragon();
     dragonKiller = true;
     return;
    }
    r = r.getNeighbors()[dir];
   }
  }
  
  public void quit(){
    alive = false;
  }
  
  public String getDirectionString(){
   return Board.DIRS[dir];
  }
  
 }



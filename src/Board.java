/**
 * Board.java
 * modified from Sebastien's Code
 * 
 */
import java.util.Random;

public class Board
{
	public static int NORTH = 0;
	public static int EAST = 1;
	public static int SOUTH = 2;
	public static int WEST = 3;
	public static String[] DIRS = {"NORTH", "EAST", "SOUTH", "WEST"};
	public static final int ROWS = 6;  // ROWS by COLS cells
	public static final int COLS = 6;
	public static final int PITP = 20; //20% of rooms have pits
	public static final int WUMPUSES = 2; //2 wumpuses
	public Room[][] rooms;
	private int[] randomOrdering;
	
	public Board ()
	{
		rooms = new Room[ROWS][COLS];

		for(int i = 0; i<ROWS; i++)
			for(int j = 0; j<COLS; j++)
				rooms[i][j] = new Room(i,j);
		for(int i = 0; i<ROWS; i++)
			for(int j = 0; j<COLS; j++)
				rooms[i][j].initNeighbors(rooms);

		//create random ordering.
		Random randomGenerator = new Random();
		randomOrdering = new int[ROWS*COLS - 1];
		for(int i = 0; i< randomOrdering.length; i++)
			randomOrdering[i] = i + 1; //start room is empty
		for(int i = 0; i<randomOrdering.length; i++){//shuffle
			int t = randomOrdering[i]; 
			int n = randomGenerator.nextInt(randomOrdering.length);
			randomOrdering[i] = randomOrdering[n];
			randomOrdering[n] = t;
		}

		//set pits, Wumpus, and gold
		int numPits = ROWS*COLS*PITP/100;
		for(int i = 0; i < numPits; i++){
			randomRoom(i).makePit();
		}
		randomRoom(numPits).spawnDragon();
		randomRoom(numPits + 1).spawnDragon();
		randomRoom(numPits + 2).depositGold();

	}

	private Room randomRoom(int i){
		int rnumber = randomOrdering[i];
		int x = rnumber/ROWS;
		int y = rnumber%ROWS;
		return rooms[x][y];
//		return rooms[1][2];
	}

	public Room getRoom(int x, int y){
		return rooms[x][y];
	}

}



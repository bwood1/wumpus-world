import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Player.java
 * modified from Sebastien's Code
 * 
 */
public class AI extends Player
{
	private Room currentRoom;
	private int dir;
	private boolean alive;
	private int arrow;
	private boolean gold;
	private boolean dragonKiller;
	private Room previousRoom;

	public AI (Room start) {
		currentRoom = start;
		dir = Board.EAST;
		alive = true;
		arrow = 2;
		gold = false;
		dragonKiller = false;
	}

	/**
	 * Looks for the perceptions in the room. If there are none
	 * set the room as visited and set all neighbors as safe.
	 * If there are perceptions then mark neighbors as having
	 * possible hazards.
	 */
	public void analyzeRoom() {
		currentRoom.setVisited();
		currentRoom.setSafe();
		ArrayList<Character> perceptions = currentRoom.perceptions();

		if (perceptions.isEmpty()) {
			markNeighorborsSafe();
		} else {
			for (char ch: perceptions) {
				switch (ch) {
				case 'S':
					markUnSafe(ch);
					break;
				case 'B':
					markUnSafe(ch);
					break;
				case 'G':
					this.gold = true;
					break;
				}
			}
		}
	}
	
	// if there is a danger in the room next to me and if there is not a perception in currentRoom
	// then remove danger and set whatever danger value to 0
	
	private void checkNeighorbors () {
		if (currentRoom.perceptions().isEmpty()) {
//			ArrayList<Character> perceptions = new ArrayList<Character>();
			for (Room rm: currentRoom.getNeighbors()) {
				rm.removeDanger('W');
				rm.removeDanger('P');
				rm.setPitRisk(0.0);
				rm.setWumpusRisk(0.0);
			}
		}
	}

	private void markUnSafe(char danger) {
		switch (danger) {
		case 'S':
			for (Room rm: currentRoom.getNeighbors()) {
				if (rm != null && !rm.getSafe()) {
					rm.setDangers('W');
					rm.setWumpusRisk(calculateRiskValue(rm));
				}
			}
			break;
		case 'B':
			for (Room rm: currentRoom.getNeighbors()) {
				if (rm != null && !rm.getSafe()) {
					rm.setDangers('S');
					rm.setPitRisk(calculateRiskValue(rm));
				}
			}
		}
	}

	private int calculateRiskValue (Room room) {
		int riskReturn = 0;
//		System.out.println("Calculate risk value of room " + room.getLocation());
		for (Room rm: room.getNeighbors()) {
			if (rm != null && rm.getVisited()) {
				for (char danger: rm.perceptions()) {
					if (danger != ' ') {
//						System.out.println("Room " + rm.getLocation() + " has perception " + danger);
						riskReturn ++ ;
					}
				}
			}
		}
		return riskReturn;
	}

	private void markNeighorborsUnVisited(Room rm) {
		for (Room room: rm.getNeighbors()) {
			if (room != null) {
//				System.out.println("Marking room " + room.getLocation() + " unvisited");
				room.unSetVisited();
			}
		}
	}

	private void markNeighorborsSafe() {
		for (Room rm: currentRoom.getNeighbors()) {
			if (rm != null) {
				rm.setSafe();
				rm.removeDanger('P');
				rm.removeDanger('W');
				rm.setPitRisk(0.0);
				rm.setWumpusRisk(0.0);
			}
		}
	}

	private boolean shootWumpus() {
		Room front = currentRoom.getNeighbors()[dir];
		if (front != null && front.getWumpusRisk() >= 3 && front.getDangers().contains('W') && this.arrow > 0) {
//			System.out.println("FIRE!!!");
			front.removeDanger('W');
			front.setWumpusRisk(0.0);
			return true;
		} else {
//			System.out.println("Hold your fire dummy!");
			return false;
		}
	}

	public char decideMove() {
		
		if (this.gold) {
			return 'g';
		} else if (shootWumpus()) {
			return 's';
		}

		Room next = lookForLeastRiskyMove();
		Room roomFront = currentRoom.getNeighbors()[dir];

		if (next == roomFront) {
			// when we take a step forward increase the visitedRisk of the new room.
			roomFront.setVisitedRisk(roomFront.getVisitedRisk() + 0.5);
			return 'f';
		} else {
			return figureOutWhichDirectionToTurn(next); // left or right
		}
	}

	/**
	 * Gets all of the neighbors of the currentRoom and returns the room that has
	 * the lowest totalRisk value.
	 * @return - the room that has the lowest risk
	 */
	private Room lookForLeastRiskyMove() {
		Room returnRoom = null;
		for (Room rm: currentRoom.getNeighbors()) {
			if (rm != null && returnRoom == null) {
				returnRoom = rm;
			} else if (rm != null && returnRoom.getTotalRisk() > rm.getTotalRisk()) {
				returnRoom = rm;
			}
		}
		return returnRoom;
	}

	/**
	 * Returns 'l' or 'r' depending on which way the AI is facing and which way the bestMove is in relation to the AI
	 * @param bestMove - The best move that was decided by lookForLeastRiskyMove()
	 * @return - 'l' or 'r' depending on the AI's orientation and which way the best room is.
	 */
	private char figureOutWhichDirectionToTurn(Room bestMove) {
		ArrayList<Room> neighbors = new ArrayList<Room>(Arrays.asList(currentRoom.getNeighbors()));
//		System.out.println("AI is facing " + dir + " and the best move is " + bestMove.getLocation());
		
		switch (dir) {
		case 0:
//			System.out.println("The AI is facing " + dir);
//			System.out.println("The best move is " + bestMove.getLocation());
			if (bestMove == neighbors.get(3)) {
//				System.out.println("turn left");
				return 'l';
			}
			break;
		case 1:
//			System.out.println("The AI is facing " + dir);
//			System.out.println("The best move is " + bestMove.getLocation());
			if (bestMove == neighbors.get(0)) {
//				System.out.println("turn left");
				return 'l';
			}
			break;
		case 2:
//			System.out.println("The AI is facing " + dir);
//			System.out.println("The best move is " + bestMove.getLocation());
			if (bestMove == neighbors.get(1)){
//				System.out.println("turn left");
				return 'l';
			}
				
			break;
		case 3:
//			System.out.println("The AI is facing " + dir);
//			System.out.println("The best move is " + bestMove.getLocation());
			
			if (bestMove == neighbors.get(2)) {
//				System.out.println("turn left");
				return 'l';
			}
			break;
			default:
//				System.out.println("turn right");
				return 'r';
				
		}
		// if current direction is dir.north and best direction is direction.west
		return 'r';
		
	}

	private Room lookForRiskyMove() {

		return null;
	}

	private void getUnStuck() {
//		System.out.println("Get unstuck");
		for (Room rm1: currentRoom.getNeighbors()) {
			if (rm1 != null) {
				rm1.unSetVisited();
				for (Room rm2: rm1.getNeighbors()) {
					if (rm2 != null) {
						rm2.unSetVisited();
					}
				}
			}
		}
	}

	private Room lookForSafeRoom() {
		//		Room returnRoom = null;
		for (Room rm: currentRoom.getNeighbors()) {
			if (rm != null && rm.getDangers().isEmpty()) {
//				System.out.println("Room " + rm.getLocation() + " is safe but has already been visited");
				return rm;
			}
		}
		return null;
	}

	private Room lookForSafeUnVisitedRoom() {
		//		Room next = neighbors[dir];
		for (Room rm: currentRoom.getNeighbors()) {
			if (rm != null && rm.getDangers().isEmpty() && !rm.getVisited()) {
//				System.out.println("Room " + rm.getLocation() + " is safe");
				return rm;
			}
		}
		return null;
	}

	/**
	 * Gets the neighorbors of the current room.
	 * If neighorbor exists (IE not the edge of the board)
	 * then the player can advance in the direction he is facing.
	 * @return
	 */
	public boolean forward(){
		Room next = currentRoom.getNeighbors()[dir]; //the next room in the direction the user is facing
		if(next==null){
			return false;
		}
		else{
			currentRoom = next;
			currentRoom.setHints();
			if (currentRoom.hasDragon() || currentRoom.isPit())
				alive = false;
		}
		return true;
	}

	public void turnLeft(){
		dir = dir==0?3:dir-1;
	}

	public void turnRight(){
		dir = dir==3?0:dir+1;
	}

	public Room getCurrentRoom(){
		return currentRoom;
	}

	public boolean hasGold(){
		return gold;
	}

	public boolean grabGold(){
		gold = currentRoom.grabGold();
		return gold;
	}

	public boolean isDragonKiller(){
		return dragonKiller;
	}

	public void resetDragonKiller(){
		dragonKiller = false;
	}

	public boolean isAlive(){
		return alive;
	}

	public boolean hasArrow(){
		if (arrow > 0)
			return true;
		return false;
	}

	public void shoot(){
		if(arrow==0)
			return;
		arrow --;
		Room r = currentRoom;
		while(r!=null){
			if(r.hasDragon()){
				r.killDragon();
				dragonKiller = true;
				return;
			}
			r = r.getNeighbors()[dir];
		}
	}

	public void quit(){
		alive = false;
	}

	public String getDirectionString(){
		return Board.DIRS[dir];
	}
}


